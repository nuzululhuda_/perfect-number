<?php
$n = $_POST['n'];
$result = [];
foreach ($n as $x) {
    $factors = [];

    for ($i = 1; $i < $x; $i++) {
        if ($x % $i == 0) {
            $factors[] = $i;
        }
    }

    $total = array_sum($factors);
    $distance = $x - $total;

    if ($distance == 0) {
        $result[] = 'Perfect';
    }else if ($distance == 1) {
        $result[] = 'Hampir';
    }else {
        $result[] = 'Bukan';
    }
}

echo "Input : <br>" . implode("<br>", $n) ."<br>";
echo "Hasil : <br>" . implode("<br>", $result) . "<br>";