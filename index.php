<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <title>Perfect Number</title>
</head>

<body>
    <form action="PerfectNumber.php" method="POST">
        <div class="container mt-4">
            <div class="form-group">
                <input type="text" name="q" placeholder="Masukkan nilai q" value="1" class="form-control">
                <button type="button" class="btn btn-primary mt-2 q">Masukkan inputan</button>
            </div>
            <div id="n">

            </div>
            <div class="form-group mt-2">
                <button type="submit" class="btn btn-primary d-none check">Cek</button>
            </div>
        </div>
    </form>
</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
<script>
    $(".q").click(function() {
        $(".check").removeClass('d-none')
        $("#n").html("")
        let q = $("input[name=q]").val()
        for (i = 0; i < q; i++) {
            $("#n").append(`
                <input type="text" class="form-control mt-2" name="n[]">
            `);
        }
    });
</script>

</html>